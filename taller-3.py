import datetime #para interpretar ISO-8601
import sys
def promedio(e,v): #parametros de la funcion: entrada y ventana
    datos = [] #todo
    fechas = [] #separo las fechas de los datos
    temperaturas = [] #separo las temps de los datos
    rangoTiempo = [] 
    promedios = [] 
    salida = [] #lista con los rangos de tiempo y promedios
    for row in e:
        row = row.split(',') #interpreta archvio csv
        datos.append(row)
    for n in range(len(datos)):
        y = datetime.datetime.strptime(datos[n][0],"%Y-%m-%dT%H:%M:%S")
        fechas.append(y)  
    for n in range(len(datos)):
        temp = []
        for x in (datos[n][1:]):
            if x == "NA" or x == "NA\n": #evitar el error que daria esto con float
                temp.append("NA")
            else: 
                temp.append(float(x)) #pasar lista de str a lista de float
        temperaturas.append(temp) 
    
    i = 0 # arranco a calcular el promedio
    while i <= (len(datos)-v):
        promAux = []
        rangoTiempo.append(((fechas[i+v-1])-(fechas[i])).total_seconds())
        for j in range(len(temperaturas[i])):
            n = 0 #para que recorra la ventana
            suma = 0
            flag = False #flag para que no apendee promedio luego del NA
            while n < v:
                if temperaturas[i+n][j]=="NA":
                    promAux.append("NA") #si temp=NA, promedio=NA
                    n = v #para que se corte ahí por tener un NA
                    flag = True
                else:
                    suma += (temperaturas[i+n][j])
                n += 1
            if not flag:
                promedio = round((suma/v),2) 
                promAux.append(promedio)
        promedios.append(promAux) #completo la lista de promedios
        i += 1
 
    for n in range(len(rangoTiempo)):
        fila =[rangoTiempo[n]] + promedios[n] #junto rangoTiempo y promedios en una lista 
        salida.append(fila)
    return salida 


entrada = open(sys.argv[1]) #pasar los parámetros por linea de comando
ventana = int(sys.argv[3])
promedio = promedio(entrada,ventana)
archivoSalida = open(sys.argv[2], 'w')
for n in range(len(promedio)):
    for j in range(len(promedio[n])):
        separador = ','
        print(promedio[n][j], file = archivoSalida , end = separador)
        if j == len(promedio[n])-1:
                    separador = '\n'
                    print('', file = archivoSalida, end = separador) 
archivoSalida.close()
 

    
        
        



